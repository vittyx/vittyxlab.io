---
categories : ["about"]
comments : false
date : "2019-01-03T00:00:00"
draft : false
slug : ""
tags : ["about"]
title : "About Me"

showpagemeta : false
---

![Vitto](/img/vittoCamera.jpg)

 Soy un Estudiante a tiempo parcial de  Ingeniería Telemática, apasionado de las nuevas tecnologias,las redes, el cloud, la seguridad y la filosofía open source. Actualmente trabajo de ingeniero QA. </br><br> En mi tiempo libre me dedico a la <a href="https://500px.com/vittyx" style="font-weight:bold">fotografía </a>, al deporte, a ver series, películas y a uno de mis hobbies favoritos: viajar.</br><br>
