---
categories : ["Web"]
comments : false
date : "2019-04-10"
draft : false
showpagemeta : true
showcomments : true
slug : ""
tags : ["Web", "Hugo", "Goa", "Markdown"]
title : "Personal website con Hugo"
description : "Creacion de una web personal con hugo y Git"
---

Como punto de partida de la web, empezaré con un post sobre la tacnologia y el porqué de mi decisión.
La Pagina Web se ha realizado con un framework llamado Hugo y Git

## Porque Hugo

![HugoLogo](/img/hugo-logo-image.jpg)

[Hugo](https://gohugo.io/) es un generador de sitios web estáticos escrito en Go, y de software libre.
Los generadores de sitios web estáticos, aunque tienen ciertas limitaciones, son una muy buena opcion para generar sitios muy rápidos y seguros. Algunas de sus [ventajas](https://gohugo.io/about/benefits/):

* **Multiplataforma**: Se puede utilizar en casi cualquier sistema operativo ([*Windows*](https://es.wikipedia.org/wiki/Windows), [*GNU/Linux*](https://es.wikipedia.org/wiki/GNU/Linux), [*macOS*](https://es.wikipedia.org/wiki/MacOS), [*FreeBSD*](https://es.wikipedia.org/wiki/FreeBSD), etc.) y arquitectura ([*x64*](https://es.wikipedia.org/wiki/X86-64), [*x86*](https://es.wikipedia.org/wiki/X86), [*ARM*](https://es.wikipedia.org/wiki/Arquitectura_ARM)).
* **Personalización**: Hugo posee varios [*themes*](http://themes.gohugo.io/) para diferentes fines y gustos. En mi caso me he decantado por [Goa](https://themes.gohugo.io/hugo-goa/), un theme minimalista y con mucho potencial de personalización.
* **Multihosting**: Al ser contenido estatico, se puede alojar en cualquier hosting web o CDN (Amazon S3, Git, Google Cloud Storage, Amazon CloudFront, …). En mi caso, empezaré alojando la web en Gitlab.
* **Soporte**: Hugo cuenta con una comunidad muy activa, un foro de soporte comunitario y un equipo de desarrolladores muy amigables.
* **Edición simple**: Aunque la curva de aprendizaje es un poco dificil, la edición de contenido y la personlalización se hace bastante facil usando [Markdown](https://daringfireball.net/projects/markdown/) y [LaTeX](https://es.wikipedia.org/wiki/LaTeX)

## Proceso

### 1. Instalación de Hugo

#### Debian and Ubuntu

```bash
sudo apt-get install hugo
```

Se puede verificar que se ha instalado la ultima [release](https://github.com/gohugoio/hugo/releases):

```bash
hugo version
```

### 2. Generación de web local

Existen [manuales](https://gohugo.io/getting-started/) donde se entra mas en detalle de los pasos a seguir.

```bash
hugo new site yourweb
```

Hugo generará la distribucion del sitio web:

```bash
archetypes/
content/
layouts/
static/
config.toml
```

Ya podemos escribir el primer post en un documento Markdown:

```bash
cd myweb
hugo new about.md
```

### 3.Importar el theme

He creado mi web basandome en el theme [Goa](https://themes.gohugo.io/hugo-goa/), sobre el que he realizado pequeñas modificaciones para adaptarlo.
![GoaTheme](/img/GoaTheme.png)

Clonar theme:

```bash
cd themes
git clone https://github.com/shenoybr/hugo-goa
cd ..
```

### 4.Modificación

Se han modificado algunos archivos para el desarrollo de la web basada en Hugo y otros para personalización del theme. Algunas de las modificaciones han sido:

* **config.toml**: Archivo principal para la configuración y despliegue del sitio web.
* **archetypes/default.md**: se han modificado algunos parametros para hacer mas facil el procesado de las funciones en los posts.
* **layouts/index.html**: se ha modificado el orden que se aplicará a los archivos que se generen posteriormente.
* **static/js/main.js**: se ha agregado codigo para adaptar las imagenes y hacer la web ***responsive***.

```js
$(document).ready(function(){
  $("img").addClass("img-rounded");
  $("img").addClass("center-block");
  $("img").addClass("img-fluid");
});
```

Por último, para verificar todo el desarrollo local, al entrar en el directorio de la web y poner el comando server, se levantará un servidor interno que escucha en el puerto 1313 con el que podremos verificar el estado del sitio web.

```bash
hugo server -t customize-hugo-goa --buildDrafts --watch
```

or

```bash
hugo server
```

### 5.Publicación de la web

Utilizamos Gitlab para alojar la web de momento.

```bash
git add --all
git commit -m "Update website"
git push
```

## Futuros pasos y mejoras
Como futuros pasos espero alojar la web en algún otro tipo de servidor y realizar algunos cambios a nivel de estética.
