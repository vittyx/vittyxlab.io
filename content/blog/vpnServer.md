---
categories : ["VPN"]
comments : false
date : "2020-02-29"
draft : false
showpagemeta : true
showcomments : true
slug : ""
tags : ["blog", "Raspberry", "VPN", "Markdown","OpenVPN"]
title : "VPN Server con Raspberry pi"
description : "Servidor VPN con Raspberry pi y OpenVPN"
---

Aprovechando la raspberry pi 3b+ podemos montarnos un servidor VPN local con OpenVPN.

## Lo primero

Ya teniendo instalados en la raspberry pi [Raspbian Lite] (https://www.raspberrypi.org/downloads/raspbian/)

Partiremos de la base de tener instalado raspbian lite y operativa la Raspberry. En caso contrario, te recomiendo que mires algunos de estos links:

* [Burning Raspbian Lite to the SD Card](https://madlab6.blogspot.com/2019/02/burning-sd-card-for-raspberry-pi-using.html)
* [Headless Setup for the Raspberry Pi](https://madlab6.blogspot.com/2019/03/headless-set-up-of-raspberry-pi-running.html)
* [Instalar Raspbian Server sin Monitor](https://www.flopy.es/instalar-raspbian-server-en-una-raspberry-pi-sin-monitor/)

Teniendo todo funcionando… actualizamos:

```bash
Sudo apt update
Sudo apt full-upgrade
```

![OpenVPNLogo](/img/openvpn.png)

## Paso 1 -Instalación de OpenVPN

Antes de empezar con la instalación deberíamos saber que es OpenVPN:
[OpenVPN](https://es.wikipedia.org/wiki/OpenVPN) es una herramienta de conectividad basada en software libre: SSL (Secure Sockets Layer), VPN Virtual Private Network (red virtual privada). OpenVPN ofrece conectividad punto-a-punto con validación jerárquica de usuarios y host conectados remotamente.

Para la instalación usamos de referencia varios manuales y páginas de internet que dejaré al final del post. El que nos ha resultado más útil ha sido el de [Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-18-04)

```bash
sudo apt install openvpn
```

## Paso 2 – Construir Certificado de Autoridad, de servidor y de usuarios

Lo ideal sería tener una CA (Autoridad de Certificación) separada, pero en este caso utilizaremos la misma Raspberry para generar los certificados de la CA, del servidor y de los clientes que se conectarán.

En la mayoría de manuales traen instrucciones de instalación de versiones anteriores, por lo que habría que tener en cuenta a la hora de hacerlo, en nuestro caso hemos instalado:

```bash
wget -P ~/ https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.6/EasyRSA-unix-v3.0.6.tgz
cd ~
tar xvf EasyRSA-unix-v3.0.6.tgz
sudo cp -r /usr/share/easy-rsa /etc/openvpn/easy-rsa
cp vars.example vars
```

Una vez copiados editamos el fichero de ejemplo que ahora se llama vars.

```bash
Sudo vim vars
```

Los parámetros de configuración que hemos metido pueden variar dependiendo de las necesidades de cada uno, por lo que hemos cogido como base unos parámetros básicos y luego hemos ido agregando. Por ejemplo:

```bash
export KEY_COUNTRY="ES"
export KEY_PROVINCE=”Madrid"
export KEY_CITY="Madrid"
export KEY_ORG="ORGANIZATION "
export KEY_EMAIL="me@myhost.mydomain"
export KEY_OU="MyOrganizationalUnit"
```

Creamos los certificados de la CA

```bash
./easyrsa init-pki
./easyrsa build-ca
```

```bash
./easyrsa gen-req servidor nopass

./easyrsa sign-req server servidor
```

Creacion de certificado de cliente…

```bash
./easyrsa gen-req guest nopass
```

Firmado

```bash
Sudo ./easyrsa sign-req client guest

```

Se han creado dos certificados guest y de los otros usuarios.
Verificamos que esta todo correcto

```bash
openssl verify -CAfile pki/ca.crt pki/issued/guest.crt

```

### Paso 3- Creamos los parámetros Diffie-Hellmann y la clave tls-auth

Generamos el Diffie-Hellmann

```bash
 sudo ./easyrsa gen-dh
```

El paso anterior puede tardar varios minutos, por lo que no te deberías asustar si ves que no avanza. Mientras te dejo un Enlace sobre [Diffie-Hellman](https://es.wikipedia.org/wiki/Diffie-Hellman) para que vayas leyendo algo.

Una vez terminado el comando anterior, generamos la clave tls

```bash
openvpn --genkey --secret ta.key
```

Esta clave ta.key deberemos colocarla en el servidor y en TODOS los clientes.
Llegados a este punto ya deberíamos tener:

* Lado Servidor: CA.crt , server.crt, server.key, dh.pem, ta.key
* Lado Cliente: CA.crt, cliente.crt,cliente.key, ta.key

Ahora nos copiamos los archivos

```bash
sudo mkdir /etc/openvpn/keys
sudo cp pki/dh.pem /etc/openvpn/keys
sudo cp pki/ca.crt /etc/openvpn/keys
sudo cp pki/private/servidor.key /etc/openvpn/keys
sudo cp pki/issued/servidor.crt /etc/openvpn/keys
sudo cp ta.key /etc/openvpn/keys
```

Descargar los archivos:
Este paso depende de cómo se haga. En nuestro caso, hemos copiado los archivos a tmp y los hemos descargado a nuestro equipo con “scp”

```bash
sudo cp /etc/openvpn/easy-rsa/pki/private/guest.key /tmp
sudo cp /etc/openvpn/easy-rsa/pki/issued/guest.crt /tmp
sudo cp /etc/openvpn/easy-rsa/pki/ca.crt /tmp
sudo cp /etc/openvpn/easy-rsa/ta.key /tmp
```

```bash
scp pi@x.154.xxx.1xx/tmp/guest.key ~/Documents/certificados
………
```

### Paso 4- Configuracion del servicio de OpenVPN

Ya tenemos los certificados y “keys” tanto de cliente como de servidor, Asi que vamos a configurar el servicio que utilizarán esas credenciales.

En el que será el servidor modifcamos el archivo de configuración.

```bash
sudo vim /etc/openvpn/server.conf
```

En el archivo vamos a configurar los parámetros:

```bash
tls-auth ta.key 0 # This file is secret
auth SHA256
dh dh.pem
```

Opcionales:

* Puerto y protocolo: cambiar el Puerto por defecto al puerto 443

```bash
port 443
proto udp #or proto tcp
```

* Redirect all traffic: forzamos que todas las conexiones usen el túnel.

```bash
push "redirect-gateway def1 bypass-dhcp"
push "dhcp-option DNS 208.67.222.222"
push "dhcp-option DNS 208.67.220.220"
```

* Certificados de credenciales: tenemos que cambiar los nombres dependiendo de los que hemos generado .

```bash
cert server.crt
key server.key
```

Cerramos y guardamos los cambios.

### Paso 5- Configuración de trafico de red.

Debemos realizar algunas configuraciones para que el servidor nos de salida a internet una vez que establezcamos la conexión.

Tenemos que habilitar ip-gordwarding:

```bash
sudo vim /etc/sysctl.conf
```

Tenemos que habilitar el net.ipv4.ip_fordward

```bash
net.ipv4.ip_forward=1
```

Verificamos:

```bash
sudo sysctl -p
```

Continuamos con las indicaciones de modifcación de las [tablas] (https://www.instructables.com/id/Raspberry-Pi-VPN-Gateway/), Muy importante no olvidarse el comando:

```bash
iptables -t nat -A POSTROUTING -s 192.X.X.X/24 -o eth0 -j MASQUERADE
```

Tenemos la explicación de los parámetros muy bien explicados en algunos tutoriales como el de[Digital Oceans]( https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-18-04), en la sección de “prerrequisitos”.

### Paso 6- Inicializamos el servicio OpenVPN

Configuramos el servicio openvpn y lo lanzamos.

```bash
sudo systemctl start openvpn
```

Verificamos el estado:

```bash
sudo systemctl status openvpn
```

Si se ha inicializado correctamente el status debería ser “Active(running)”. Tambien deberíamos verificar la interfaz tun0

```bash
ip addr show tun0
```

Por ultimo habilitamos el servicio para que se inicie automáticamente

```bash
sudo systemctl enable openvpn
```

### Paso 7- Configuración de el/los cliente/s

En el cliente instalamos openvpn y creamos un archivo de configuración. Para ello primero copiamos el archivo de ejemplo para modificarlo o directamente creamos uno y metemos los parámetros.

```bash
sudo cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf /etc/openvpn/client/guest.opvn
```

Editamos las líneas con los siguientes parámetros:

```bash
proto upd
ca ca.crt
cert client.crt
key client.key
tls-auth ta.key 1
auth SHA256
```

Ya tendriamos configurado el cliente tambien solamente quedaría probar la conexión:

```bash
sudo openvpn --config /etc/openvpn/client/guest.ovpn
```

Configuramos cliente y servidor y probamos desde el cliente…

```bash
sudo openvpn --config /etc/openvpn/client/guest.opvn
```

### Paso 8 – Opcional – Certificados en el archivo opvn

Una vez que comprobamos que el funcionamiento es correcto, podemos incluir los certificados dentro del archivo de configuración del archivo ovpn. [Openvpn](https://samuel.dalesjo.net/?p=375) permite la inclusión de archivos en la configuración principal de la --ca, --cert, --dh, --extra-certs, --key, --pkcs12, --secret y --tls-auth opciones, [Otro Ejemplo](https://www.enmimaquinafunciona.com/pregunta/4699/generar-un-perfil-de-openvpn-para-cliente-usuario-importar)

```bash
<cert> -----BEGIN CERTIFICATE----- [...] -----END CERTIFICATE----- </cert>
.........
```

### Paso 8 – Próximos pasos

 Los próximos pasos serian configurar dentro de un dominio propio la actualización de Dynamic ddns para que la configuración no tenga que ser manual.

### Referencias:

* [madlab5](https://madlab5.blogspot.com/2019/03/creating-home-vpn-server-using.html)
* [atomicobject](https://spin.atomicobject.com/2019/04/07/openvpn-raspberry-pi/)
* [Digital Oceans](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-18-04)
* [thegeePub](https://www.thegeekpub.com/239681/setup-a-raspberry-pi-vpn-server/)
* [howtoForge](https://www.howtoforge.com/tutorial/how-to-install-openvpn-server-and-client-with-easy-rsa-3-on-centos-8/)
* [juncotic](https://juncotic.com/openvpn-easyrsa-3-montando-la-vpn/)
