---
categories : ["CV","LaTex"]
comments : false
date : "2021-01-19"
draft : false
showpagemeta : true
showcomments : true
slug : ""
tags : ["blog", "cv", "LaTex", "Markdown","AWS","WebSite"]
title : "CV con Latex y deploy en S3"
description : "Actualizar CV en Latex, gestionarlo en gitlab y desplegarlo en S3"
---

He tenido que actualizar el cv y mi última versión era uno que tenía en formato ai(adobe ilustrator), el problema era cambiar todo el diseño por simplemente agregar o quitar líneas y por el hecho que cada vez uso menos Windows, por lo que he optado por hacer el cv en LaTex y experimentar un poco con ello.
De esta forma, se podría mantener actualizado y simplemente "compilarlo" y generar un nuevo cv. Por otro lado y para no tener que estar subiéndolo a mi página web(en mi caso la tengo alojada en un bucket S3 de AWS) de forma manual, he automatizado la "subida" directa después de la generación del archivo pdf.

## Lo primero

Para introducirme en LaTex un buen recurso que he encontrado fue esta [WEB](https://ugr.es/~mmartins/material/latex_basico.pdf), para editarlo de forma online al principio sin tener que instalar nada use [Overleaf](https://www.overleaf.com/), para alguien que no tiene mucha idea y es novato es bastante intuitivo y ademas te permite realizar cambios en tiempo real y compilarlos y ver el resultado.

Otros links interesantes para ir metiendose en el tema:

* [Introducción a LaTex webdelprogramador](https://www.lawebdelprogramador.com/pdf/18593-Introduccion-a-LaTeX.html)
* [Tutorial de LaTeX: Introducción básica](http://www.mat.uda.cl/hsalinas/cursos/2008/latex/doc-tutorial-latex.pdf)

![LaTex Logo](/img/latex.png)

## Paso 1 -Instalación de LaTex y template para modificar.

Antes de empezar con la instalación deberíamos saber ya que es LaTex y algunas diferencias entre LaTeX, pdfTeX, XeTeX, LuaTeX and ConTeXt, en [OverLeaf](https://www.overleaf.com/learn/latex/Articles/The_TeX_family_tree:_LaTeX,_pdfTeX,_XeTeX,_LuaTeX_and_ConTeXt) lo explican bastante bien.

Para la instalacion basica de LaTex:

```bash
sudo apt-get install texlive-latex-base
```

Recomiendan tambien instalar fuentes extras y paquetes extras:

```bash
sudo apt-get install texlive-fonts-recommended
sudo apt-get install texlive-fonts-extra
sudo apt-get install texlive-latex-extra
```

Hay una lista bastante buena dentro de [OverLeaf](https://es.overleaf.com/latex/templates/tagged/presentation) donde se puede ver en tiempo real y hacer algunas pruebas. Uno de los que mas me ha gustado es el de [FoutySeconds](https://github.com/PandaScience/FortySecondsCV).

![FourtySecondsCV](/img/FourtySecondsCV.jpg)

En su repositorio de GitHub nos advierten:

## Requirements

You need to compile your document with XeLaTeX or LuaLaTeX in order to have
the latest Font Awesome icons (`fontawesome5`) and Academicons. If you still
want to compile with pdfLaTeX for whatever reason, ~~FortySecondsCV will fall
back to the older icon package (`fontawesome`), where some icons look
different and some others are not even included~~ Academicons won't be
available.

Por lo que seria recomendable instalar XeLatex

```bash
sudo apt-get install texlive-xetex
```

## Paso 2 – Modificación del CV y generación de repositorio en git

Hacemos un fork, lo clonamos o descgarmaos y generamos un repositorio propio de git.

```bash
git init
git add .
git commit -m "Initial commit"

```

Creamos un archivo ..gitlab-ci.yml

```yaml
stages:
  - createPDF
  - deploy

createPDF:
  stage: createPDF
  image: registry.gitlab.com/islandoftex/images/texlive:latest
  script:
    - xelatex cv.tex
  artifacts:
    paths:
    - cv.pdf

```

Hemos creado dos stages, uno para generar el archivo y el de deploy que utilizaremos para "subir" y actualizar el archivo de cv de la web(en mi caso esta en bucket de AWS)

En mi caso, he decidido utilizar la imagen de [islandoftex](https://gitlab.com/islandoftex/images/texlive). *This repository provides dockerfiles for [TeX Live](http://tug.org/texlive/) repositories (full installation with all packages but without documentation). It also provides the necessary tooling to execute common helper tools (e.g. Java for Arara, Perl for Biber and Xindy, Python for Pygments).
Please note that we only provide selected historical releases and one image
corresponding to the latest release of TeX Live (tagged latest).*

La otra opcion seria crear y configurar un una imagen de docker propia para utilizar y compliar los archivos:

* [Three Ways to Create Dockernized LaTeX Environment](https://towardsdatascience.com/three-ways-to-create-dockernized-latex-environment-2534163ee0c4)
* [Building a LaTeX Docker image](https://gordonlesti.com/building-a-latex-docker-image/)

Ya tenemos el archivo creado y el template modificado. Procedemos a la "subida"

```bash
git add .
git commit -m "Added .gitlab-ci.yml file"
```

### Si hemos creado el repositorio Local y queremos sincronizarlo con git

Creamos un proyecto en Gitlab y asociamos el repositorio que tenemos local.

```bash
git remote add origin https://gitlab.com/username/projectname.git
```

Por último hacemos pushç

```bash
git push -u origin master
```

Deberia el CI/CD dashboard mostrarnos que se ha generado correctamente.

![pipeline](/img/pipelinePDF.png)

Podriamos descargarnos el archivo generado desde *Job artifacts*

![Artifact](/img/artifact.png)

### Paso 3- Sincronizamos para la subida al bucket S3

![gitlabToS3](/img/gitlabToS3.jpg)

En el archivo *.gitlab-ci.yml* agregamos las lineas de deploy

```yaml
 deploy:
    stage: deploy
    image: cgbaker/alpine-hugo-aws:latest
    only:
    - master
    script:
    - aws s3 sync . s3://$S3_BUCKET_NAME/ --exclude='*' --include='cv.pdf'
```

Para que funcione debemos acordarnos de declarar las variables en los ajustes de CD/CI del proyecto.

![variables](/img/variables.png)

Para refrescar un poco dejo unos links:
La otra opcion seria crear y configurar un una imagen de docker propia para utilizar y compliar los archivos:

* [GitLab CI: Deployment & Environments](https://about.gitlab.com/blog/2016/08/26/ci-deployment-and-environments/)
* [Sync Files to S3 Bucket Via AWS CLI](https://initialcommit.com/blog/sync-files-s3-bucket-aws-cli)

Una vez terminamos volvemos a probar y el CI/CD dashboard mostrarnos:

![pipelineFinal](/img/pipelineFinal.png)

Un ejemplo del job seria:

![jobFinal](/img/job.png)

### Referencias:

* [GitlabCDCI](https://about.gitlab.com/blog/2016/08/26/ci-deployment-and-environments/)
* [SynS3Gitlab](https://initialcommit.com/blog/sync-files-s3-bucket-aws-cli)
* [Introduction to CDCI Gitlab](https://levelup.gitconnected.com/a-gentle-introduction-to-gitlab-ci-cd-4be1d3ea7f19)
* [upload a file to AWS S3](https://medium.com/devops-with-valentine/how-to-upload-a-file-to-aws-s3-from-gitlab-ci-using-aws-cli-v1-bad7e9c1fb6a)
* [Support LATEX sections in GitLab](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/13180)
* [Compiling (La)TeX files automatically with GitLab CI](https://tex.stackexchange.com/questions/459484/compiling-latex-files-automatically-with-gitlab-ci)
